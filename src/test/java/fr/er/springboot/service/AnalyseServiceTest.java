/**
 * 
 */
package fr.er.springboot.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class AnalyseServiceTest {
	
	@Autowired
	AnalyseService analyseService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ProjectService projectService;
	
	Calendar calendar;
	
	User user;
	
	User user2;
	
	Project project;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		user = new User("blabla@pblbb.com");
		user2 = new User("mielam@ganzn.com");
		project = new Project("MJbndmjbadm","fr.test.stag","no idea");
		
		calendar = Calendar.getInstance();
		calendar.set(2018, 05, 8, 12, 50);
		
		projectService.save(project);
		userService.save(user);
		userService.save(user2);
	}

	/**
	 * Test method for {@link fr.er.springboot.service.AnalyseService#save(fr.er.springboot.entity.Analyse)}.
	 */
	@Test
	public void testSaveAnalyse() {
		Analyse analyse = new Analyse("Error", 1, 6, 4,new Timestamp(calendar.getTime().getTime()) , 10, 2, project, user);
		
		analyseService.save(analyse);
	
		
		
	}

	/**
	 * Test method for {@link fr.er.springboot.service.AnalyseService#one(fr.er.springboot.entity.Analyse)}.
	 */
	@Test
	public void testOneAnalyse() {
		Analyse analyse = new Analyse("Error", 1, 20, 4,new Timestamp(calendar.getTime().getTime()) , 50, 7, project, user2);
		
		Analyse found = analyseService.save(analyse);
	
		assertEquals(found, analyse);
		
	}

	/**
	 * Test method for {@link fr.er.springboot.service.AnalyseService#saveAll(java.util.List)}.
	 */
	@Test
	public void testSaveAllListOfAnalyse() {
	
		List<Analyse> analyseList = new ArrayList<>();
		analyseList.add(new Analyse("Okay", 10, 0, 20,new Timestamp(calendar.getTime().getTime()) , 70, 7, project, user2));
		analyseList.add(new Analyse("Error", 1, 20, 4,new Timestamp(calendar.getTime().getTime()) , 50, 7, project, user));
		
		
		List<Analyse> found = analyseService.saveAll(analyseList);
		
		assertNotNull(found);
	}
	
	
	@Test
	public void testTriggerTechnicalRuntimeExceptionOnSave() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		analyseService.save(null);
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveAll() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullList);
		analyseService.saveAll(null);
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnOne() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		analyseService.one(null);
		
	}

}
