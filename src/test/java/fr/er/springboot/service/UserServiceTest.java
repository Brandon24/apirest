/**
 * 
 */
package fr.er.springboot.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.er.springboot.entity.User;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class UserServiceTest {
	@Autowired
	private UserService userService;
	
	private User user;
	
	private User saveTest;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		 user = new User("jesaispas@gmail");
		
		 saveTest = new User("olaaas@gmail");
		
		 userService.save(saveTest);
	}

	/**
	 * Test method for {@link fr.er.springboot.service.UserService#save(fr.er.springboot.entity.User)}.
	 */
	@Test
	public void testSaveUser() {
		User foundEntity =	userService.save(user);
		
		assertNotNull(foundEntity);
		assertEquals(foundEntity, user);
	}

	/**
	 * Test method for {@link fr.er.springboot.service.UserService#one(fr.er.springboot.entity.User)}.
	 */
	@Test
	public void testOneUser() {
		
		User found = userService.one(saveTest);
		
		
		assertNotNull(found);
		assertEquals(found, saveTest);
		
	}

	/**
	 * Test method for {@link fr.er.springboot.service.UserService#saveAll(java.util.List)}.
	 */
	@Test
	public void testSaveAllListOfUser() {
		List<User> list = new ArrayList<>();
		
		list.add(new User("aaaaaa"));
		list.add(new User("msqlslmq"));
		list.add(new User("wqsasa"));
		
		List<User> second = userService.saveAll(list);
	
		assertNotNull(second);
		assertEquals(3, second.size());
	}
	
	@Test
	public void testTriggerTechnicalRuntimeExceptionOnSave() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		userService.save(null);
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveAll() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullList);
		userService.saveAll(null);
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnOne() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		userService.one(null);
		
	}

}
