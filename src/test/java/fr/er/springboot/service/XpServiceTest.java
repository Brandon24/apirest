package fr.er.springboot.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.er.springboot.entity.Xp;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class XpServiceTest {

	@Autowired
	XpService xpService;
	
	Xp xp;
	Xp xpOne;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() throws Exception {
		xp  = new Xp();
		xp.setProject(null);
		xp.setUser(null);
		Date date = new Date(118,04,04);
		xp.setLastAnalysis(new Timestamp(date.getTime()));
		xp.setXp(145);
		
		// On save cette objet pour tester si on arrive bien a le recuperer par la suite
		xpOne  = new Xp();
    	xpOne.setProject(null);
    	xpOne.setUser(null);
    	xpOne.setLastAnalysis(new Timestamp(date.getTime()));
    	xpOne.setXp(145);
    	xpOne = xpService.save(xpOne);
		
		
	}

	@Test
	public void testSaveXp() {
		System.out.println(xp);
		Xp xp2 = xpService.save(xp);
		
		assertNotNull(xp2);
	}

	@Test
	public void testOneXp() {
		Xp xp2 = xpService.one(xpOne);
		System.out.println();
		assertNotNull(xp2);
	}

	@Test
	public void testSaveAllListOfXp() {
		List<Xp> xpList = new ArrayList<>();
		
		List<Xp> emptyList = new ArrayList<>();

		xpList.add(new Xp(200, null, null, null));
		xpList.add(new Xp(250, null, null, null));
		xpList.add(new Xp(220, null, null, null));
		xpList.add(new Xp(260, null, null, null));
	
		xpList  = xpService.saveAll(xpList);
		
		assertNotNull(xpList);
		assertNotEquals(xpList, emptyList);
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveWithNullObject() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		xpService.save(null);

	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveAll() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullList);
		xpService.saveAll(null);
		
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnOne() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject );
		xpService.one(null);
	
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveAllWithEmptyList() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullList );
		xpService.saveAll(new ArrayList<Xp>());
		
		
	}

}
