/**
 * 
 */
package fr.er.springboot.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.er.springboot.entity.Project;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

/**
 * @author erudo
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class ProjectServiceTest {

	@Autowired
	ProjectService projectService;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	/**
	 * Test method for {@link fr.er.springboot.service.ProjectService#save(fr.er.springboot.entity.Project)}.
	 */
	@Test
	public void testSaveProject() {
		Project project = new Project("JE SAIS PAS","fr.test.stag","Le Projet d'un stagiaire");
		projectService.save(project);
	}

	/**
	 * Test method for {@link fr.er.springboot.service.ProjectService#one(fr.er.springboot.entity.Project)}.
	 */
	@Test
	public void testOneProject() {
	
		Project	project1 = new Project("UN ID UNIQUE","fr.test.stag","Le Projet d'un stagiaire");
		Project foundEntity  = projectService.save(project1);
		
		assertNotNull(foundEntity);
		assertEquals(foundEntity, project1);
	}

	/**
	 * Test method for {@link fr.er.springboot.service.ProjectService#saveAll(java.util.List)}.
	 */
	@Test
	public void testSaveAllListOfProject() {
		List<Project> projectList = new ArrayList<>();
		projectList.add(new Project("ID DIFFERENT","fr.test.stag","Le Projet d'un stagiaire"));
		projectList.add(new Project("Sandwich","fr.test.stag","La Mie caline"));
		projectList.add(new Project("carrefouf.cle","fr.test.stag","Carrefour"));
		
		projectService.saveAll(projectList);
		
		
	}
	
	@Test
	public void testTriggerTechnicalRuntimeExceptionOnSave() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		projectService.save(null);
		
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnSaveAll() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullList);
		projectService.saveAll(null);
	}
	
	@Test
	public void testTriggerOnTechnicalRuntimeExceptionOnOne() {
		thrown.expect(TechnicalRuntimeException.class);
		thrown.expectMessage(ErrorKey.nullObject);
		projectService.one(null);
		
	}
	

}
