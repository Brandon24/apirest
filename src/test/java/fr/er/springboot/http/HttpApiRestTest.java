	/**
 * 
 */
package fr.er.springboot.http;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;

/**
 * @author erudo
 *
 */
// 

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class HttpApiRestTest {

	@Value("${user.token}")
	String token;
	HttpApiRest httpApiRest;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		httpApiRest = new HttpApiRest();
		httpApiRest.setUsername(token);
	}

	/**
	 * Test method for {@link fr.er.springboot.http.HttpApiRest#getAnalyseFromAPI()}.
	 */
	@Test
	public void testGetAnalyseFromAPI() {
		List<Analyse> lAnalyses = httpApiRest.getAnalyseFromAPI();
		assertTrue(lAnalyses.size() != 0);
	
	}

	/**
	 * Test method for {@link fr.er.springboot.http.HttpApiRest#getUserFromAPI()}.
	 */
	@Test
	public void testGetUserFromAPI() {
		List<User> lUsers = httpApiRest.getUserFromAPI();
		assertTrue(lUsers.size() != 0);
	}

	/**
	 * Test method for {@link fr.er.springboot.http.HttpApiRest#getProjectFromAPI()}.
	 */
	@Test
	public void testGetProjectFromAPI() {
		List<Project> listProject = httpApiRest.getProjectFromAPI();
		assertTrue(listProject.size() != 0);
	}

}
