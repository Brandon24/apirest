/**
 * 
 */
package fr.er.springboot.check;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

/**
 * @author erudo
 *
 */
public class CheckDataTest {
	List<User> listUser;
	List<Project> listProject;
	List<Analyse> listAnalyse;
	
	List<User> secondlistUser;
	List<Project> secondlistProject;
	List<Analyse> secondlistAnalyse;
	
	CheckData checkData = new CheckData();
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		
		listUser = new ArrayList<User>();
		listProject = new ArrayList<Project>();
		listAnalyse = new ArrayList<Analyse>();
		
		secondlistUser = new ArrayList<User>();
		secondlistProject = new ArrayList<Project>();
		secondlistAnalyse = new ArrayList<Analyse>();
		
		User user = new User("User");
		User user1 = new User("Antoine");
		User user2 = new User("Clavier");
		
		Project project = new Project("wwwAAAwwww", "fr.project.secret", "Project secret");
		Project project1 = new Project("UNKNOWN", "fr.natacha.project", "Natacha Project");
		Project project2 = new Project("macd", "fr.martine.Junit", "Martine apprend le Junit");
		
		
		Date date = new Date(118, 02, 04);
		Date date1 = new Date(118, 03, 04);
		Date date2 = new Date(118, 04, 04);
		
		Timestamp timestamp = new Timestamp(date.getTime());
		Timestamp timestamp2 = new Timestamp(date1.getTime());
		Timestamp timestamp3 = new Timestamp(date2.getTime());
		
		Analyse analyse = new Analyse("OK", 0, 0, 0, timestamp3, 0, 0, project, user);
		Analyse analyse1 = new Analyse("ERROR", 418, 40, 0, timestamp2, 0, 200, project1, user1);
		Analyse analyse2 = new Analyse("ERROR", 0, 60, 0,timestamp, 30, 0, project2, user2);
		
		listUser.add(user);
		listUser.add(user1);
		listUser.add(user2);
		
		secondlistUser.add(user);
		secondlistUser.add(user);
		secondlistUser.add(user);
		secondlistUser.add(user1);
		secondlistUser.add(user2);
		
		listProject.add(project);
		listProject.add(project1);
		listProject.add(project2);
		
		secondlistProject.add(project);
//		secondlistProject.add(project);
		
		
		
		listAnalyse.add(analyse);
		listAnalyse.add(analyse1);
		listAnalyse.add(analyse2);
		
		secondlistAnalyse.add(analyse);
	}

	/**
	 * Test method for {@link fr.er.springboot.check.CheckData#checkSameData(java.util.List, java.util.List)}.
	 */
	@Test
	public void testCheckSameDataForUserWithNullList() {
	      thrown.expect(TechnicalRuntimeException.class);
	      thrown.expectMessage(ErrorKey.nullList);
	      
	      checkData.checkIdenticalData(null, null);
	      
		
		
	}
	
	
	/**
	 * Test method for {@link fr.er.springboot.check.CheckData#checkSameData(java.util.List, java.util.List)}.
	 */  
	@Test
	public void testCheckSameDataForUserWithDifferenTypeOftList() {
//		  thrown.expect(TechnicalRuntimeException.class);
//	      thrown.expectMessage(ErrorKey.ObjectType);
	      
	      checkData.checkIdenticalData(new ArrayList<>(), new ArrayList<Integer>());
	      
		
	}
	

	/**
	 * Test method for {@link fr.er.springboot.check.CheckData#getNotIdenticalDataOnArraylist(java.util.List, java.util.List)}.
	 * @throws IdenticalData 
	 */
	
	@Test
	public void testGetNotIdenticalDataOnArraylist()  {
		
		List<User> resultUsers = checkData.checkIdenticalData(listUser, secondlistUser);
		
		
		List<Project> resultProject = checkData.checkIdenticalData(listProject, secondlistProject);
		  
	
		List<Analyse> resultAnalyses = checkData.checkIdenticalData(listAnalyse, secondlistAnalyse);

		checkData.checkIdenticalData(listAnalyse, new ArrayList<Analyse>());
		
		// les éléments sont présent dans les deux listes
		assertEquals(0, resultUsers.size());
		
		assertEquals(2, resultProject.size());
		
		assertEquals(2, resultAnalyses.size());
		
	}



}
