/**
 * 
 */
package fr.er.springboot.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author erudo
 *
 */
public class UserTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}


	/**
	 * Test method for {@link fr.er.springboot.entity.User#equals(fr.er.springboot.entity.User)}.
	 */
	@Test
	public void testEqualsUser() {
		User user = new User("FabriceLeProDuBowling@bowling.com");
		User user1 = new User("FabriceLeProDuBowling@bowling.com");
		
		assertEquals(user, user1);
	}
	
	
	@Test
	public void testNotEqualsUser() {
		User user = new User("FabriceLeProDuBowling@bowling.com");
		User user1 = new User("assistantenatacha@windev.com");
		
		assertNotEquals(user, user1);
	}

}
