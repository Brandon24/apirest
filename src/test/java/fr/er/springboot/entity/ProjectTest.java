/**
 * 
 */
package fr.er.springboot.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author erudo
 *
 */
public class ProjectTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}


	/**
	 * Test method for {@link fr.er.springboot.entity.Project#equals(fr.er.springboot.entity.Project)}.
	 */
	@Test
	public void testNotEqualsProject() {
		Project project = new Project("wwwww", "fr.erduo.test", "Developpement d'un projet de test");
		Project expected =  new Project("aw47q", "fr.erudo.test", "Second projet de test");
		assertNotEquals(expected, project);
	}
	
	
	@Test
	public void testEqualsProject() {
		Project project = new Project("wwwww", "fr.erduo.test", "Developpement d'un projet de test");
		Project expected =  new Project("wwwww", "fr.erduo.test", "Developpement d'un projet de test");
		
		assertEquals(expected, project);
	}

}
