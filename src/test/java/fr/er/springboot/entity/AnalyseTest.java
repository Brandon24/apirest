/**
 * 
 */
package fr.er.springboot.entity;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

/**
 * @author erudo
 *
 */
public class AnalyseTest {
 
	Analyse analyse;
	Analyse analyse1;
	Analyse analyse2;
	         
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		User user = new User("User");
		User user1 = new User("Antoine");
		User user2 = new User("Clavier");
		
		Project project = new Project("wwwAAAwwww", "fr.project.secret", "Project secret");
		Project project1 = new Project("UNKNOWN", "fr.natacha.project", "Natacha Project");
		Project project2 = new Project("macd", "fr.martine.Junit", "Martine apprend le Junit");
		
		
		Date date = new Date(118, 02, 04);
		Date date1 = new Date(118, 03, 04);
		Date date2 = new Date(118, 04, 04);
		
		Timestamp timestamp = new Timestamp(date.getTime());
		Timestamp timestamp2 = new Timestamp(date1.getTime());
		Timestamp timestamp3 = new Timestamp(date2.getTime());
		
		analyse = new Analyse("OK", 0, 0, 0, timestamp, 0, 0, project, user);
		analyse1 = new Analyse("OK", 418, 40, 0, timestamp, 0, 200, project, user1);
		analyse2 = new Analyse("ERROR", 0, 60, 0,timestamp, 30, 0, project2, user2);
		
	}


	/**
	 * Test method for {@link fr.er.springboot.entity.Analyse#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		
		assertEquals(analyse, analyse1);

	}
	
	/**
	 * Test method for {@link fr.er.springboot.entity.Analyse#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsObject() {
		
		assertNotEquals(analyse, analyse2);

	}

}
