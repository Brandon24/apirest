package fr.er.springboot.resource;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.Project;
import fr.er.springboot.service.ProjectService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ProjectRessourceTest {

	@Autowired
	ProjectResource projectResource;
	
	@Autowired
	ProjectService projectService;

	
	Project project1;


	/**
	 * @throws java.lang.Exception
	 */
	
	@Before
	public void setUp() throws Exception {
			
		project1 = new Project("UN ID UNIQUE","fr.test.stag","Le Projet d'un stagiaire");
		projectService.save(project1);	 
	}

	@Test
	public void testFindAllProjects() {
		ResponseEntity<List<Project>> response = projectResource.findAllProjects();
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void testFindOneProject() {
		
		assertNotNull(projectResource.findOneProject(project1.getName()));
		
	}

}
