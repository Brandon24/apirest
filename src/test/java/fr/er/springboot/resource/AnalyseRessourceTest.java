package fr.er.springboot.resource;

import static org.junit.Assert.assertSame;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.service.AnalyseService;
import fr.er.springboot.service.ProjectService;
import fr.er.springboot.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AnalyseRessourceTest {
	
	@Autowired
	AnalyseRessource analyseRessource;
	
	@Autowired
	AnalyseService analyseService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	UserService userService;
	

	User user;
	User user2;
	User user3;
	
	Project project1;
	
	Project project2;
	
	Analyse analyse;
	
	

	/**
	 * @throws java.lang.Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1988);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Timestamp time = new Timestamp(cal.getTime().getTime());
		
		user = new User("lesFamilleDesBugs");
		user2 = new User("leCompilateurPasContent");
		user3 = new User("leTesteurFou");
		
		project1 = new Project("UN ID UNIQUE","fr.test.stag","Le Projet d'un stagiaire");
		
		analyse = new Analyse("Error", 100, 2, 1, time, -20, 50, project1, user);
		
		
		userService.save(user);
		projectService.save(project1);
		analyseService.save(analyse);
		

		 
	}

	@Test
	public void testFindAllAnalyses() {
		ResponseEntity<List<Analyse>> response = analyseRessource.findAllAnalyses();
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
	}

}
