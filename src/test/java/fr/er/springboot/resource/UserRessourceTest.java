/**
 * 
 */
package fr.er.springboot.resource;

import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.User;
import fr.er.springboot.service.UserService;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRessourceTest {
	
	@Autowired
	private	UserRessource userRessource;
	@Autowired
	private	UserService userService;
	
	private User user;
	private User user2;
	private User user3;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		
		user = new User("lesFamilleDesBugs");
		user2 = new User("leCompilateurPasContent");
		user3 = new User("leTesteurFou");
		
		userService.save(user);
		userService.save(user2);
		userService.save(user3);
		
		
	}

	/**
	 * Test method for {@link fr.er.springboot.resource.UserRessource#findAllUsers()}.
	 */
	@Test
	public void testFindAllUsers() {
		ResponseEntity<List<User>> response = userRessource.findAllUsers();
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
	}

}
