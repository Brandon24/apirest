/**
 * 
 */
package fr.er.springboot.resource;

import static org.junit.Assert.assertSame;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.entity.Xp;
import fr.er.springboot.service.ProjectService;
import fr.er.springboot.service.UserService;
import fr.er.springboot.service.XpService;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class XpRessourceTest {

	@Autowired
	XpRessource xpRessource;
	
	@Autowired
	XpService xpService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	UserService userService;
	
	Project project1;
	
	Xp xp;
	Xp xp2;
	Xp xp3;
	
	User user;
	User user2;
	User user3;
	
	Analyse analyse;
	
	

	/**
	 * @throws java.lang.Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		
		
		


				
		project1 = new Project("UN ID UNIQUE","fr.test.stag","Le Projet d'un stagiaire");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1988);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Timestamp time = new Timestamp(cal.getTime().getTime());
		
		user = new User("lesFamilleDesBugs");
		user2 = new User("leCompilateurPasContent");
		user3 = new User("leTesteurFou");
		
		 xp = new Xp(400,time,user,project1);
		 xp2 = new Xp(200,time,user2,project1);
		 xp3 = new Xp(100,time,user3,project1);
		 
		 userService.save(user);
		 userService.save(user2);
		 userService.save(user3);
		 
		 projectService.save(project1);
		 
		 xpService.save(xp);
		 xpService.save(xp2);
		 xpService.save(xp3);
		 
	}

	/**
	 * Test method for {@link fr.er.springboot.resource.XpRessource#findAllXp()}.
	 */
	@Test
	public void testFindAllXp() {
		ResponseEntity<List<Xp>> response =  xpRessource.findAllXp();
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
	}

	/**
	 * Test method for {@link fr.er.springboot.resource.XpRessource#findAllXpByProject(java.lang.String)}.
	 */
	@Test
	public void testFindAllXpByProject() {
		
		
		ResponseEntity<List<Xp>> response =  xpRessource.findAllXpByProject(project1.getId_project());
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
		
		
	}

	/**
	 * Test method for {@link fr.er.springboot.resource.XpRessource#findAllXpByUser(java.lang.String)}.
	 */
	@Test
	public void testFindAllXpByUser() {
		ResponseEntity<List<Xp>> response =  xpRessource.findAllXpByUser(user.getLogin());
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
	}

	/**
	 * Test method for {@link fr.er.springboot.resource.XpRessource#findOneXpByUserAndProject(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testFindAllXpByUserAndProject() {
		
		ResponseEntity<Xp> response =  xpRessource.findOneXpByUserAndProject(user.getLogin(),project1.getId_project());
		
		assertSame(response.getStatusCode(), HttpStatus.OK);
		
	}

}
