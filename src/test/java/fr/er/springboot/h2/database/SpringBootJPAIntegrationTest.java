/**
 * 
 */
package fr.er.springboot.h2.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.entity.Xp;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;
import fr.er.springboot.service.AnalyseService;
import fr.er.springboot.service.ProjectService;
import fr.er.springboot.service.UserService;
import fr.er.springboot.service.XpService;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SpringBootJPAIntegrationTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private XpService xpService;
	
	@Autowired
	private AnalyseService analyseService;
	
	Project project;
	
	User user;
	

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	
		 project  = projectService.save(new Project("awzaesasasd45s", "fr.springboot.secret.project", "SecretProject"));
		 user =  userService.save(new User("nouvelleUtilisateur@test.com"));
	}
	
	 @Test
	    public void givenUserEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
		 	User user =  userService.save(new User("jeSuisUnTest@test.com"));
	    
		 	User foundEntity = userService.one(user);
		 	
		 	assertNotNull(foundEntity);
		 	assertEquals(user, foundEntity);
		 	
	    }
	
	 @Test
	    public void givenAnalyseEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
		 	 user =  userService.save(new User("jeSuisUnTest@test.com"));
	    
		 	User foundEntity = userService.one(user);
		 	
		 	assertNotNull(foundEntity);
		 	assertEquals(user, foundEntity);
		 	
	    }
	 
	 
	 @Test
	    public void givenProjectEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
		 	Project project = projectService.save(new Project("awzaed45s", "fr.springboot.secret.project", "SecretProjectByTheStagiaire"));
		 	
		 	Project foundEntity = projectService.one(project);
		 	
		 	assertNotNull(foundEntity);
		 	assertEquals(project, foundEntity);
		 	
		 

		 	
	    }
	 
	 
	 
	 @Test
	    public void givenXpEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
		 	System.out.println(user);
		 	Date date = new Date(118, 05, 04);
		 	Xp xp = xpService.save(new Xp(200,new Timestamp(date.getTime()),user,project));
		 	
		 	Xp foundEntity  = xpService.one(xp);
		 	System.out.println(foundEntity);
		 	
		 	assertNotNull(foundEntity);
		 	assertEquals(xp, foundEntity);

	    }
	 
	 @Test
	    public void givenStatistiquesEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
		 	User user =  userService.save(new User("jeSuisUnTest@test.com"));
	    
		 	User foundEntity = userService.one(user);
		 	
		 	assertNotNull(foundEntity);
		 	assertEquals(user, foundEntity);
		 	
	    }

}
