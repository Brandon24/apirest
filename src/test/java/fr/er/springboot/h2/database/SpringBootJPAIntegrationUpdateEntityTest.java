/**
 * 
 */
package fr.er.springboot.h2.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.service.AnalyseService;
import fr.er.springboot.service.ProjectService;
import fr.er.springboot.service.UserService;
import fr.er.springboot.service.XpService;

/**
 * @author erudo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SpringBootJPAIntegrationUpdateEntityTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private XpService xpService;
	
	@Autowired
	private AnalyseService analyseService;
	
	private Project project;
	
	private User user;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	
	@Test
    public void givenProjectEntityRepository_whenUpdateAndRetreiveEntity_thenOK() {
	 	Project project = projectService.save(new Project("awzaed45s", "fr.springboot.secret.project", "SecretProjectByTheStagiaire"));
	 			
	 			
    
	 	Project foundEntity = projectService.one(project);
	 	
	 	assertNotNull(foundEntity);
	 	assertEquals(project, foundEntity);
	 	System.out.println(foundEntity.getName());
	 	foundEntity.setName("Hello World ! s");

	 	List<Project> pp = new ArrayList<>();
	 	
	 	pp.add(foundEntity);
	 	
	 	pp.add(new Project("ccc","nn","aaa"));
	 	
	 	projectService.saveAll(pp);
    }

}
