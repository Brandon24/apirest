/**
 * 
 */
package fr.er.springboot.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author erudo
 *
 */
@Entity
@Table(name="userr")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	
	@Id
	@JsonProperty("login")
	private String login;
	

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	
	
	/**
	 * @param login
	 * 
	 */
	public User(String login) {
		this.login = login;
	
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [login=" + login + "]";
	}
	
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof User && this.getLogin().equals(((User)obj).getLogin());
	}
	
}
