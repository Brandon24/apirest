/**
 * 
 */
package fr.er.springboot.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author erudo
 *
 */
@Entity
public class Statistiques {
	
	@Id
	@Column(name = "analysis_date")
	private Timestamp analysisDate;

	private int nbVulnerabilitie;
	private int nbCode_smell;
	private int nbTestUnitaire;
	private int nbBugs;
	@ManyToOne
	private User user;
	@ManyToOne
	private Project project;
	
	public Statistiques() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param analysis_date
	 * @param nbVulnerabilitie
	 * @param nbCode_smell
	 * @param nbTestUnitaire
	 * @param nbBugs
	 * @param user
	 * @param project
	 * (Timestamp, int, int, int, User, Project) 
	 */
	public Statistiques(Timestamp analysis_date, int nbVulnerabilitie, int nbCode_smell, int nbTestUnitaire, int nbBugs,
			User user, Project project) {
		this.analysisDate = analysis_date;
		this.nbVulnerabilitie = nbVulnerabilitie;
		this.nbCode_smell = nbCode_smell;
		this.nbTestUnitaire = nbTestUnitaire;
		this.nbBugs = nbBugs;
		this.user = user;
		this.project = project;
	}

	/**
	 * @return the nbBugs
	 */
	public int getNbBugs() {
		return nbBugs;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}



	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}



	/**
	 * @return the nbVulnerabilitie
	 */
	public int getNbVulnerabilitie() {
		return nbVulnerabilitie;
	}

	/**
	 * @return the nbCode_smell
	 */
	public int getNbCode_smell() {
		return nbCode_smell;
	}

	/**
	 * @return the analysis_date
	 */
	public Timestamp getAnalysis_date() {
		return analysisDate;
	}

	/**
	 * @return the nbTestUnitaire
	 */
	public int getNbTestUnitaire() {
		return nbTestUnitaire;
	}

	/**
	 * @param analysis_date the analysis_date to set
	 */
	public void setAnalysis_date(Timestamp analysis_date) {
		this.analysisDate = analysis_date;
	}

	/**
	 * @param nbVulnerabilitie the nbVulnerabilitie to set
	 */
	public void setNbVulnerabilitie(int nbVulnerabilitie) {
		this.nbVulnerabilitie = nbVulnerabilitie;
	}

	/**
	 * @param nbCode_smell the nbCode_smell to set
	 */
	public void setNbCode_smell(int nbCode_smell) {
		this.nbCode_smell = nbCode_smell;
	}

	/**
	 * @param nbTestUnitaire the nbTestUnitaire to set
	 */
	public void setNbTestUnitaire(int nbTestUnitaire) {
		this.nbTestUnitaire = nbTestUnitaire;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @param nbBugs the nbBugs to set
	 */
	public void setNbBugs(int nbBugs) {
		this.nbBugs = nbBugs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Statistiques [analysis_date=" + analysisDate + ", nbVulnerabilitie=" + nbVulnerabilitie
				+ ", nbCode_smell=" + nbCode_smell + ", nbTestUnitaire=" + nbTestUnitaire + ", nbBugs=" + nbBugs
				+ ", user=" + user + ", project=" + project + "]";
	}
	
	
	
	

}
