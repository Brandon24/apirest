package fr.er.springboot.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="xp")
public class Xp {
	
	@Id
	@GeneratedValue
	private int id;
	@Column(name="xp")
	private int xp;
	
	private Timestamp lastAnalysis;
	
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Project project;
	
	/**
	 * @param xp
	 * @param lastAnalysis
	 * @param user
	 * @param project
	 */
	public Xp(int id,int xp, Timestamp lastAnalysis, User user, Project project) {
		this.id = id;
		this.xp = xp;
		this.lastAnalysis = lastAnalysis;
		this.user = user;
		this.project = project;
	}
	
	 

	/**
	 * @param xp
	 * @param lastAnalysis
	 * @param user
	 * @param project
	 */
	public Xp(int xp, Timestamp lastAnalysis, User user, Project project) {
		this.xp = xp;
		this.lastAnalysis = lastAnalysis;
		this.user = user;
		this.project = project;
	}



	public Xp() {
		
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the xp
	 */
	public int getXp() {
		return xp;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Xp) {
			Xp xp = (Xp) obj;
			return xp instanceof Xp 
					&& this.getUser().equals(xp.getUser()) 
					&& this.getProject().equals(xp.getProject()) 
					&& this.getLastAnalysis().equals(xp.getLastAnalysis());

		}
		return false;
	}
	
	
	public boolean equalsUserAndProject(Xp obj) {

			Xp xp =  obj;
			return xp instanceof Xp 
					&& this.getUser().equals(xp.getUser()) 
					&& this.getProject().equals(xp.getProject());



	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param xp the xp to set
	 */
	public void setXp(int xp) {
		this.xp = xp;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @return the lastAnalysis
	 */
	public Timestamp getLastAnalysis() {
		return lastAnalysis;
	}

	/**
	 * @param lastAnalysis the lastAnalysis to set
	 */
	public void setLastAnalysis(Timestamp lastAnalysis) {
		this.lastAnalysis = lastAnalysis;
	}
	
	
	
}
