/**
 * 
 */
package fr.er.springboot.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author erudo
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="project")
public class Project {

	@Id
	@Column(name="id_project")
	@JsonProperty("id")
	private String id;
	
	@Column(name="key")
	@JsonProperty("key")
	private String key;
	
	@Column(name="name")
	@JsonProperty("name")
	private String name; 
	
	public Project() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id_project
	 * @param key
	 * @param name
	 */
	public Project(String id_project, String key, String name) {
		this.id = id_project;
		this.key = key;
		this.name = name;
	}


	/**
	 * @return the id_project
	 */
	public String getId_project() {
		return id;
	}

	/**
	 * @param id_project the id_project to set
	 */
	public void setId_project(String id_project) {
		this.id = id_project;
	}

	/**
	 * @return the id
	 */

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Project [id_project=" + id + ", key=" + key + ", name=" + name + "]";
	}



	@Override
	public boolean equals(Object obj) {
		
		return obj instanceof Project && this.getId_project().equals(((Project) obj).getId_project());
	}
	

}
