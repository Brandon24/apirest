/**
 * 
 */
package fr.er.springboot.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author erudo
 *
 */
@Entity
@Table(name="analyser")
public class Analyse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	 
	@Column(name = "quality_branch")
	private String qualityBranch;
	
	@Column(name = "bugs")
	private int bugs = 0;
	
	@Column(name = "vulnerabilitie")
	private int vulnerabilitie = 0;
	
	@Column(name = "code_smell")
	private int codeSmell = 0;
	
	@Column(name = "analysis_date")
	private Timestamp analysisDate;
	
	@Column(name = "total_test_unitaire")
	private int totalTestUnitaire = 0;
	
	@Column(name = "coverage")
	private double coverage = 0.0;
	
	private String bugsMessage;
	
	private String vulnerabilityMessage;
	
	private String codeSmellMessage;
	
	@ManyToOne
	private Project project;
	
	@ManyToOne
	private User user;
	

	/**
	 * @param quality_branch
	 * @param bugs
	 * @param vulnerabilitie
	 * @param code_smell
	 * @param analysis_dateDate
	 * @param total_test_unitaire
	 * @param coverage
	 * @param commit
	 * @param project
	 * @param user
	 */
	
	public Analyse(String quality_branch, int bugs, int vulnerabilitie, int code_smell, Timestamp analysis_dateDate,
			int total_test_unitaire, double coverage, Project project, User user) {
		this.qualityBranch = quality_branch;
		this.bugs = bugs;
		this.vulnerabilitie = vulnerabilitie;
		this.codeSmell = code_smell;
		this.analysisDate = analysis_dateDate;
		this.totalTestUnitaire = total_test_unitaire;
		this.coverage = coverage;
		this.project = project;
		this.user = user;
	}
	
	public Analyse() {
	}

	/**
	 * @return the quality_branch
	 */
	public String getQuality_branch() {
		return qualityBranch;
	}

	/**
	 * @return the bugs
	 */
	public int getBugs() {
		return bugs;
	}

	/**
	 * @return the vulnerabilitie
	 */
	public int getVulnerabilitie() {
		return vulnerabilitie;
	}

	/**
	 * @return the code_smell
	 */
	public int getCode_smell() {
		return codeSmell;
	}

	/**
	 * @return the analysis_dateDate
	 */
	public Timestamp getAnalysis_dateDate() {
		return analysisDate;
	}

	/**
	 * @return the total_test_unitaire
	 */
	public int getTotal_test_unitaire() {
		return totalTestUnitaire;
	}

	/**
	 * @return the coverage
	 */
	public double getCoverage() {
		return coverage;
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * @param total_test_unitaire the total_test_unitaire to set
	 */
	public void setTotal_test_unitaire(int total_test_unitaire) {
		this.totalTestUnitaire = total_test_unitaire;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Analyse [id=" + id +  ", quality_branch=" + qualityBranch + ", bugs="
				+ bugs + ", vulnerabilitie=" + vulnerabilitie + ", code_smell=" + codeSmell + ", analysis_date="
				+ analysisDate + ", total_test_unitaire=" + totalTestUnitaire + ", coverage=" + coverage
				+ ", project=" + project + ", user=" + user + "]";
	}
	
	
	@Override
	public boolean equals(Object objet) {
		
		if (objet instanceof Analyse) {
			Analyse obj = (Analyse) objet;
			return this.getAnalysis_dateDate().equals(obj.getAnalysis_dateDate()) && this.getProject().equals(obj.getProject());
		}
		 
		return false;
	}

	/**
	 * @param bugs the bugs to set
	 */
	public void setBugs(int bugs) {
		this.bugs = bugs;
	}

	/**
	 * @param vulnerabilitie the vulnerabilitie to set
	 */
	public void setVulnerabilitie(int vulnerabilitie) {
		this.vulnerabilitie = vulnerabilitie;
	}

	/**
	 * @param code_smell the code_smell to set
	 */
	public void setCode_smell(int code_smell) {
		this.codeSmell = code_smell;
	}

	/**
	 * @return the analysis_date
	 */
	public Timestamp getAnalysis_date() {
		return analysisDate;
	}

	/**
	 * @return the bugsMessage
	 */
	public String getBugsMessage() {
		return bugsMessage;
	}

	/**
	 * @return the vulnerabilityMessage
	 */
	public String getVulnerabilityMessage() {
		return vulnerabilityMessage;
	}

	/**
	 * @return the codeSmellMessage
	 */
	public String getCodeSmellMessage() {
		return codeSmellMessage;
	}

	/**
	 * @param quality_branch the quality_branch to set
	 */
	public void setQuality_branch(String quality_branch) {
		this.qualityBranch = quality_branch;
	}

	/**
	 * @param analysis_date the analysis_date to set
	 */
	public void setAnalysis_date(Timestamp analysis_date) {
		this.analysisDate = analysis_date;
	}

	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(double coverage) {
		this.coverage = coverage;
	}

	/**
	 * @param bugsMessage the bugsMessage to set
	 */
	public void setBugsMessage(String bugsMessage) {
		this.bugsMessage = bugsMessage;
	}

	/**
	 * @param vulnerabilityMessage the vulnerabilityMessage to set
	 */
	public void setVulnerabilityMessage(String vulnerabilityMessage) {
		this.vulnerabilityMessage = vulnerabilityMessage;
	}

	/**
	 * @param codeSmellMessage the codeSmellMessage to set
	 */
	public void setCodeSmellMessage(String codeSmellMessage) {
		this.codeSmellMessage = codeSmellMessage;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	
	
	
}
