package fr.er.springboot.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import fr.er.springboot.entity.User;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

public class Authors {
	@JsonProperty("authors")
	private List<String> listAuthor;
	public Authors() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the nameAuthor
	 */
	public List<String> getListAuthor() {
		return listAuthor;
	}
	
	/**
	 * 
	 * @return a list 
	 */
	public List<User> getAllUser(){
		if (listAuthor.size() == 0) {
			new TechnicalRuntimeException(ErrorKey.nullList);
		}
		
		List<User> listUser = new ArrayList<User>();
		
		for (int i = 0; i < getListAuthor().size(); i++) {
			listUser.add(new User(getListAuthor().get(i)));
		}
		return listUser;
	}

}
