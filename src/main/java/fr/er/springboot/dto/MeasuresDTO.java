package fr.er.springboot.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeasuresDTO {
	@JsonProperty("metric")
	private String metric;
	
	@JsonProperty("value")
	private String value;
	
	@JsonProperty("periods")
	private List<PeriodsDTO> periodsList;
	
//	@JsonProperty("history")
//	private List<HistoryDTO> history;

	public MeasuresDTO() { 
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Measures [metric=" + getMetric() + ", value="+ getValue() + "]";
	}

	/**
	 * @return the metric
	 */
	public String getMetric() {
		return metric;
	}

	/**
	 * @return the periodsList
	 */
	public List<PeriodsDTO> getPeriodsList() {
		return periodsList;
	}

	/**
	 * @return the history
	*/
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) {
			
			return this.getMetric().equals(obj);
		}
		return false;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	

}
