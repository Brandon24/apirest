/**
 * 
 */
package fr.er.springboot.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author erudo
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class IssueDTO {
	
	@JsonProperty("key")
	private String key;
	
	@JsonProperty("project")
	private String project;
	
	@JsonProperty("creationDate")
	private Timestamp creationDate;
	
	@JsonProperty("updateDate")
	private Timestamp updateDate;
	
	@JsonProperty("author")
	private String author;
	
	@JsonProperty("type")
	private String type;
	

	/**
	 * 
	 */
	public IssueDTO() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}


	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}


	/**
	 * @return the creationDate
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}


	/**
	 * @return the updateDate
	 */
	public Timestamp getUpdateDate() {
		return updateDate;
	}


	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}


	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	


}
