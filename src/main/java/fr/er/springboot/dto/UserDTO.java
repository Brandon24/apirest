/**
 * 
 */
package fr.er.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author erudo
 *
 */
public class UserDTO {


	@JsonProperty("id")
	private int id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("membersCount")
	private int membersCount;
	@JsonProperty("default")
	private boolean group_default;

	/**
	 * 
	 */


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GroupUserDTO [ id=" + id
				+ ", name=" + name + ", description=" + description + ", membersCount=" + membersCount
				+ ", group_default=" + group_default + "]";
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the membersCount
	 */
	public int getMembersCount() {
		return membersCount;
	}

	/**
	 * @return the group_default
	 */
	public boolean isGroup_default() {
		return group_default;
	}


}
