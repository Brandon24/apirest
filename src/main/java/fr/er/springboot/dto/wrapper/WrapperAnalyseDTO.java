/**
 * 
 */
package fr.er.springboot.dto.wrapper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.er.springboot.constant.Metric;
import fr.er.springboot.dto.IssueDTO;
import fr.er.springboot.dto.MeasuresDTO;
import fr.er.springboot.dto.PagingDTO;
import fr.er.springboot.dto.PeriodsDTO;
import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.key.UtilityKeys;


/**
 * 
 * @author erudo
 * 
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WrapperAnalyseDTO {

//	@JsonProperty("paging")
	@JsonIgnore
	private PagingDTO paging;
	
	@JsonProperty("measures")
	private List<MeasuresDTO> measures;
	
	public WrapperAnalyseDTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the paging
	 */
	public PagingDTO getPaging() {
		return paging;
	}

	/**
	 * @return the measures
	 */
	public List<MeasuresDTO> getMeasures() {
		return measures;
	}

	/**
	 * 
	 * @param wrapperIssueDTO
	 * @param project
	 * @return a analyse List 
	 * 
	 * Json Format : 
	 * {
            "measures": [
                {
                    "metric": "bugs",
                    "value": "2",
                    "component": "lorem ipsum",
                    "bestValue": false
                }
         }
	 */
	public Analyse getTest(WrapperIssueDTO wrapperIssueDTO,Project project){
		Analyse analyse = new Analyse();
		IssueDTO commitInformations = wrapperIssueDTO.getListIssueDTO().get(0);
		List<Analyse> analyseList = new ArrayList<>();

		
		analyse.setUser(new User(commitInformations.getAuthor()));
		analyse.setProject(project);
		analyse.setAnalysis_date(commitInformations.getUpdateDate());
		

			for (MeasuresDTO measuresDTO : measures) {
				
					
					// Information changement des statistiques
					// L'api ne renvoie toujours qu'une seul valeur
					
						
					if (measuresDTO.getMetric().equalsIgnoreCase(Metric.COVERAGE.toString())) {
						analyse.setCoverage(Double.parseDouble(measuresDTO.getValue()));
						
					}else if (measuresDTO.getMetric().equalsIgnoreCase(Metric.ALERT_STATUS.toString())) {
						analyse.setQuality_branch(measuresDTO.getValue());
						
					}
					
					if (measuresDTO.getPeriodsList() == null) {
						System.out.println("Null");
						continue;
					}
					
					PeriodsDTO period = measuresDTO.getPeriodsList().get(0);
					
					// Si il y a eu des changements de statistique
					if (measuresDTO.getPeriodsList().size() > 0 ) {
						
						if (measuresDTO.getMetric().equalsIgnoreCase(Metric.TESTS.toString())) {
							analyse.setTotal_test_unitaire(Integer.parseInt(period.getValue()));
							
						}else if (measuresDTO.getMetric().equalsIgnoreCase(Metric.BUGS.toString())) {
							analyse.setBugs(Integer.parseInt(period.getValue()));
							
						}else if (measuresDTO.getMetric().equalsIgnoreCase(Metric.VULNERABILITIES.toString())) {
							analyse.setVulnerabilitie(Integer.parseInt(period.getValue()));
							
						}else if (measuresDTO.getMetric().equalsIgnoreCase(Metric.CODE_SMELLS.toString())) {
							analyse.setCode_smell(Integer.parseInt(period.getValue()));
							
						}
					}
				
			
			}
			System.out.println("End");
			
		return analyse;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyseDTO [paging=" + paging + ", measures=" + measures + "]";
	}

}
