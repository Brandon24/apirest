/**
 * 
 */
package fr.er.springboot.dto.wrapper;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import fr.er.springboot.dto.PagingDTO;
import fr.er.springboot.dto.UserDTO;

/**
 * @author erudo
 *
 */

public class WrapperGroupUserDTO {
	@JsonProperty("groups")
	private UserDTO userDTO[];
	
	@JsonProperty("paging")
	private PagingDTO PagingDTO;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GroupUserDTO [userDTO=" + Arrays.toString(userDTO) + "]";
	}

	/**
	 * @return the userDTO
	 */
	public UserDTO[] getUserDTO() {
		return userDTO;
	}

	/**
	 * @return the pagingDTO
	 */
	public PagingDTO getPagingDTO() {
		return PagingDTO;
	}
	

}

 