/**
 * 
 */
package fr.er.springboot.dto.wrapper;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.er.springboot.dto.PagingDTO;
import fr.er.springboot.entity.Project;

/**
 * @author erudo
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WrapperProjectsDTO {
	@JsonProperty("paging")
	private PagingDTO pagingDTO;
	
	@JsonProperty("projects")
	private List<Project> project;

	/**
	 * @return the pagingDTO
	 */
	public PagingDTO getPagingDTO() {
		return pagingDTO;
	}

	/**
	 * @return the project
	 */
	public List<Project> getProject() {
		return project;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WrapperProjectsDTO [pagingDTO=" + pagingDTO + ", project=" + project + "]";
	}

}
