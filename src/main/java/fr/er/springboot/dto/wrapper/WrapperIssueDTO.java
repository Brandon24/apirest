/**
 * 
 */
package fr.er.springboot.dto.wrapper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.text.Utilities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.er.springboot.dto.IssueDTO;
import fr.er.springboot.dto.PagingDTO;
import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.User;
import fr.er.springboot.key.UtilityKeys;

/**
 * @author erudo
 *
 */
@JsonIgnoreProperties(ignoreUnknown =  true)
public class WrapperIssueDTO {
	
	@JsonProperty("paging")
	PagingDTO pagingDTO;
	
	@JsonProperty("issues")
	List<IssueDTO> listIssueDTO;
	
	
	public WrapperIssueDTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the pagingDTO
	 */
	public PagingDTO getPagingDTO() {
		return pagingDTO;
	}

	/**
	 * @return the listIssueDTO
	 */
	public List<IssueDTO> getListIssueDTO() {
		return listIssueDTO;
	}
	
}
