/**
 * 
 */
package fr.er.springboot.dto.wrapper;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.er.springboot.entity.User;

/**
 * @author erudo
 *
 */

@JsonIgnoreProperties(ignoreUnknown=true)
public class WrapperUserDTO {
	
	@JsonProperty("users")
	private List<User> user;
	

	public WrapperUserDTO() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the user
	 */
	public List<User> getUser() {
		return user;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WrapperUserDTO [user=" + user + "]";
	}

	
}
