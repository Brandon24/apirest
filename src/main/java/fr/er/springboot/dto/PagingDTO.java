package fr.er.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PagingDTO {
	@JsonProperty("pageIndex")
	private int pageIndex;
	@JsonProperty("pageSize")
	private int pageSize;
	@JsonProperty("total")
	private int total;

	public PagingDTO() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Paging [pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", total=" + total + "]";
	}
	

	/**
	 * @return the pageIndex
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

	
	
	
	
	
}
