/**
 * 
 */
package fr.er.springboot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author erudo
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PeriodsDTO {
	
	@JsonProperty("value")
	private String value;
	
	/**
	 * 
	 */
	public PeriodsDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	
	

}
