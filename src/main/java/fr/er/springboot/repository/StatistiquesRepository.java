/**
 * 
 */
package fr.er.springboot.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Statistiques;


/**
 * @author erudo
 *
 */
@Repository
public interface StatistiquesRepository extends JpaRepository<Statistiques, Timestamp>{
	
	

	 String sql = "SELECT 0 as \"id\",0 as \"coverage\",analyser.user_login, analyser.project_id_project,\n" + 
			"analyser.quality_branch,\n" + 
			"MAX(analyser.analysis_date) as \"analysis_date\",\n" + 
			"SUM(total_test_unitaire) as \"nb_test_unitaire\",\n" + 
			"SUM(vulnerabilitie) as \"nb_vulnerabilitie\",\n" + 
			"SUM(bugs) as \"nb_bugs\",\n" + 
			"SUM(code_smell) as \"nb_code_smell\"\n" + 
			"from userr inner JOIN analyser\n" + 
			"on userr.login = analyser.user_login\n" + 
			"INNER JOIN project on analyser.project_id_project = project.id_project";
	
	String groupByUserAndProject = " GROUP BY analyser.user_login,analyser.project_id_project,analyser.quality_branch";
	
	String sqlStatistisquesByUserAndProject = sql + groupByUserAndProject;
	
	String clauseDate =" where analysis_date > ?1";
	
	String sqlQueryMaxDate = "select MAX(analysis_date) as \"analysis_date\",\n" + 
			"nb_bugs,\n" + 
			"nb_code_Smell,\n" + 
			"nb_Test_Unitaire,\n" + 
			"nb_Vulnerabilitie,\n" + 
			"project_id_project,\n" + 
			"user_login\n" + 
			" from statistiques group by analysis_date order by analysis_date desc LIMIT 1";
	
	
	
	@Query(value = sqlStatistisquesByUserAndProject, nativeQuery= true)
	List<Statistiques> statistiquesList();
	
	@Query(value = sql + clauseDate + groupByUserAndProject, nativeQuery= true)
	List<Statistiques> statistiquesListSinceThisDate(Timestamp timestamp);
	
	@Query(value = sqlQueryMaxDate,nativeQuery = true)
	Statistiques findMaxDate();
	
}
