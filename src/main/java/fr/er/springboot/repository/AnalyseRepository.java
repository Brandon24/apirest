package fr.er.springboot.repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.er.springboot.entity.Analyse;

@Repository
public interface AnalyseRepository extends JpaRepository<Analyse, Integer> {

	@Query("Select a from Analyse a where a.analysisDate = ?1")
	Analyse findAnalyseByDate(Timestamp date);
	
}
