package fr.er.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.er.springboot.entity.Xp;

@Repository
public interface XpRepository extends JpaRepository<Xp, Integer>{	

	Xp findByUser_loginAndProject_id(String user,String idProject);
	
	List<Xp> findByUser_login(String user);
	
	List<Xp> findByProject_id(String idProject); 
}
