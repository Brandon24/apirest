package fr.er.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import fr.er.springboot.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String>{
	
	
	Project findByName(String name);  
 
}
