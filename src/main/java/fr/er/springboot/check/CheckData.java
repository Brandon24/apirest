/**
 * 
 */
package fr.er.springboot.check;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.er.springboot.entity.Xp;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

/**
 * @author erudo
 *
 */
public class CheckData {

	/**
	 * 
	 */
	public CheckData() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @param <T>
	 * @param dataFromDatabase
	 * @param dataFromApi
	 * @return 
	 * les données venant de l'api s'il n'y a aucune  donnée en base sinon 
	 * elle calcule les données qui ne sont pas présente dans la base
	 * @throws TechnicalRuntimeException
	 * 
	 */
	public <T> List<T> checkIdenticalData(List<T> dataFromDatabase, List<T> dataFromApi){
		
		if (dataFromApi == null || dataFromDatabase == null) {
			throw new TechnicalRuntimeException(ErrorKey.nullList);
		// si les données de la base sont nul alors on retourne directement la liste des données de l'api
		}else if (dataFromDatabase.size() == 0) {
			return dataFromApi;
		}
			
		return getNotIdenticalDataOnArraylist(dataFromDatabase, dataFromApi);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param dataFromDatabe
	 * @param dataFromApi
	 * @return une liste contenant les objets qui ne sont pas présent dans la base
	 */
	public <T> List<T> getNotIdenticalDataOnArraylist(List<T> dataFromDatabe, List<T> dataFromApi){
		List<T> stackData =  new ArrayList<T>();
		for (int i = 0; i < dataFromDatabe.size(); i++) {
			// On ajoute si les données ne sont pas les mêmes pour renvoyer un arraylist juste avec les données a insérer en base
			
			boolean actualObjectIsPresent = false;
			
			for (int j = 0; j < dataFromApi.size(); j++) {
	
				// Si l'objet est présent alors on arrête la boucle 
				if (dataFromDatabe.get(i).equals(dataFromApi.get(j))) {
					actualObjectIsPresent = true;
					break;
					
				}
			}
	
			// si l'objet n'est pas présent alors on l'ajoute a notre list
			if (actualObjectIsPresent == false) {
				stackData.add(dataFromDatabe.get(i));
			}	
		
		}
		
		return stackData;
	}
		
}
