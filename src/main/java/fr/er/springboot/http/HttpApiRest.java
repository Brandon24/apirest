/**
 * 
 */
package fr.er.springboot.http;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.er.springboot.dto.Authors;
import fr.er.springboot.dto.wrapper.WrapperAnalyseDTO;
import fr.er.springboot.dto.wrapper.WrapperGroupUserDTO;
import fr.er.springboot.dto.wrapper.WrapperIssueDTO;
import fr.er.springboot.dto.wrapper.WrapperProjectsDTO;
import fr.er.springboot.dto.wrapper.WrapperUserDTO;
import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.util.Transformer;

/**
 * @author erudo
 * Them links are based on this documentation :
 * 	- https://sonarcloud.io/web_api/
 */
public class HttpApiRest extends HttpBasic {
	// allow to get all users with the id of the group
	private final String URL_ALL_GROUP_USERS="https://sonarcloud.io/api/user_groups/search?organization=erudo";
	private final String URL_ALL_USERS= "https://sonarcloud.io/api/user_groups/users?id=";
	
	private final String URL_TEST = "https://sonarcloud.io/api/issues/authors?organization=erudo";
	
	private final String URL_ALL_PROJECT="https://sonarcloud.io/api/projects/search_my_projects";
	
	private Transformer transformer =  new Transformer();
	
	public HttpApiRest() {
	}


	public List<Analyse> getAnalyseFromAPI() {
		String baseURL="https://sonarcloud.io/api/";
		
		String urlIssue =  "issues/search?branch=develop&projects=";
		String measure = "measures/search?projectKeys=";
		
		//correspond a la donnée que l'on veut récuperer
		String metricKeys= "&metricKeys=tests,code_smells,bugs,vulnerabilities,alert_status,coverage";
		
		
		
		List<Project> projects = getProjectFromAPI();
		
		List<Analyse> listAnalyses = new ArrayList<Analyse>(); 
			
		for (Project  currentProject : projects) { 
			/**
			 * Recupération des données 
			 * 
			 * Format for responseAllMetrics : 
			 * 		sonarcloud.io/api/measures/search?projectKeys=nomProjet&metricsKeys=(example)tests
			 * 
			 * Check this link : https://sonarcloud.io/web_api/
			 * To understand how to work them links
			 */
			
			String responseAllMetrics = getDataFromApi(baseURL + measure + currentProject.getKey() + metricKeys);
			String responseAllIssueByProject = getDataFromApi(baseURL + urlIssue + currentProject.getKey());
			
			// liste des données (tests,bugs..)
			WrapperAnalyseDTO analyseDTO = (WrapperAnalyseDTO) transformer.transformJsonToDTO(responseAllMetrics, WrapperAnalyseDTO.class);
			// liste de l'historique des commit	
			WrapperIssueDTO wrapperIssueDTO = (WrapperIssueDTO) transformer.transformJsonToDTO(responseAllIssueByProject, WrapperIssueDTO.class);
			
			listAnalyses.add(analyseDTO.getTest(wrapperIssueDTO, currentProject));
			
		//	System.out.println(listAnalyses);
			
		} 
		
		System.out.println("ANALYSE SIZE  : " + listAnalyses);
		
		 
		 
		return listAnalyses;
	}
	
		
	public List<User> getUserFromAPI() {
		String json = getDataFromApi(URL_TEST);
		Authors authors = (Authors) transformer.transformJsonToDTO(json, Authors.class);
		
		String json2 = getDataFromApi(URL_ALL_GROUP_USERS);
		WrapperGroupUserDTO wrapperGroupUserDTO = (WrapperGroupUserDTO) transformer.transformJsonToDTO(json2, WrapperGroupUserDTO.class);
		
		List<User> userList = new ArrayList<>();
		
		for (int i = 0; i < wrapperGroupUserDTO.getUserDTO().length; i++) {
			
			String json3 = getDataFromApi(URL_ALL_USERS + wrapperGroupUserDTO.getUserDTO()[i].getId());
			
			WrapperUserDTO wrapperUserDTO = (WrapperUserDTO) transformer.transformJsonToDTO(json3, WrapperUserDTO.class);
			userList.addAll(wrapperUserDTO.getUser());
			
		}
			userList.addAll(authors.getAllUser());
			
		return userList; 
	}
	

	public List<Project> getProjectFromAPI() {
		String json = getDataFromApi(URL_ALL_PROJECT);
		WrapperProjectsDTO projectsDTO = (WrapperProjectsDTO) transformer.transformJsonToDTO(json, WrapperProjectsDTO.class);
		
		return projectsDTO.getProject();
	}
	
	
}

