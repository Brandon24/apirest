package fr.er.springboot.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;


public abstract class HttpBasic {
	private final String method = "GET";
	private String username;
	private String password=""; 
/**
	 * @param tokensrc/main/resources 
	 */  
	public HttpBasic() {
		
	}
	
	/**
	 * 
	 * @param url
	 * @return true if the url is correct
	 */
	private boolean checkURL(String url) {
	
		try {
			URL url2 = new URL(url);
		}catch(MalformedURLException e) {	
			e.printStackTrace();
			
			return false;
		}
		
		return true;
		
	}
	
	private String convertDataToString(InputStream inputStream) {
		
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		
		try {
			while ((length = inputStream.read(buffer)) != -1) {
				
			    result.write(buffer, 0, length); 
			}
			
			 return result.toString("UTF-8");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return "";
		
	}
	
	
	protected String getDataFromApi(String url) {
		String jsonString ="";
		
		if (checkURL(url)) {
		
			try {
				HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
				String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes());  //Java 8
				connection.setRequestProperty("Authorization", "Basic " + encoded);
				connection.setRequestMethod(method);
				connection.connect();
				jsonString = convertDataToString(connection.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return jsonString;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password
	 */	

}
