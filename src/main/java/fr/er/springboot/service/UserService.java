package fr.er.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.repository.UserRepository;
import fr.er.springboot.service.model.ModelService;



/**
 * @author erudo
 *
 */
@Service
public class UserService extends ModelService<User>{
	@Autowired
	private UserRepository userRepository;

	@Override
	public User save(User object) {
		// TODO Auto-generated method stub
		isValidObject(object);
		return userRepository.save(object);
	}

	@Override
	public User one(User user) {
		isValidObject(user);
		return userRepository.findById(user.getLogin()).get();
	}

	@Override
	public List<User> saveAll(List<User> T) {
		isValidList(T);
		
		return userRepository.saveAll(T);
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	
}
