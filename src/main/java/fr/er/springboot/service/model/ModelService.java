package fr.er.springboot.service.model;

import java.util.List;

import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;

public abstract class ModelService<T> {
	
	
	public abstract T save(T object);
	public abstract T one(T id);
	
	public abstract List<T> saveAll(List<T> list);
	
	public abstract List<T> findAll();
	
	protected void isValidObject(Object object) {
		if (object == null ) {
			throw new TechnicalRuntimeException(ErrorKey.nullObject);
		}
	}
	
	protected void isValidList(List<T> object) {
		if (object == null ) {
			throw new TechnicalRuntimeException(ErrorKey.nullList);
		}else if (object.size() == 0) {
			throw new TechnicalRuntimeException(ErrorKey.nullList);
		}
	}
	
}
