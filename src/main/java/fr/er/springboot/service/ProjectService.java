package fr.er.springboot.service;
/**
 * @author erudo
 *
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import fr.er.springboot.entity.Project;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.http.HttpApiRest;
import fr.er.springboot.key.ErrorKey;
import fr.er.springboot.repository.ProjectRepository;
import fr.er.springboot.service.model.ModelService;

@Service
public class ProjectService extends ModelService<Project> {
	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project save(Project object) {
		// TODO Auto-generated method stub
		isValidObject(object);
		
		return projectRepository.save(object);
	}

	@Override
	public Project one(Project project) {
		isValidObject(project);
		return projectRepository.findById(project.getId_project()).get();
	}



	@Override
	public List<Project> saveAll(List<Project> T) {
		isValidList(T);
		
		return projectRepository.saveAll(T);
	}



	@Override
	public List<Project> findAll() {
		// TODO Auto-generated method stub
		return  projectRepository.findAll();
	}
	
	public Project findOneProjectByName(String name) {
		
		isValidObject(name);
		
		return projectRepository.findByName(name);
	}

}
