/**
 * 
 */
package fr.er.springboot.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.er.springboot.entity.Statistiques;
import fr.er.springboot.repository.StatistiquesRepository;
import fr.er.springboot.service.model.ModelService;

/**
 * @author erudo
 *
 */
@Service
public class StatistiquesService extends ModelService<Statistiques> {

	@Autowired
	private StatistiquesRepository statistiquesRepository;
	/**
	 * 
	 */
	public StatistiquesService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Statistiques save(Statistiques object) {
		// TODO Auto-generated method stub
		isValidObject(object);
		
		return statistiquesRepository.save(object);
	}


	@Override
	public Statistiques one(Statistiques id) {
		isValidObject(id);
		return statistiquesRepository.findById(id.getAnalysis_date()).get();
	}

	@Override
	public List<Statistiques> saveAll(List<Statistiques> T) {
		
		isValidList(T);

		return statistiquesRepository.saveAll(T);
	}

	@Override
	public List<Statistiques> findAll() {
		// TODO Auto-generated method stub
		return statistiquesRepository.findAll();
	}
	
	public List<Statistiques> createStatistiqueWithTableAnalyseWithoutDate() {
		return statistiquesRepository.statistiquesList();

	}
	
	public List<Statistiques> createStatistiqueWithTableAnalyseWithDate(Timestamp timestamp) {	
		return statistiquesRepository.statistiquesListSinceThisDate(timestamp);

	}
	
	public Statistiques findMaxDate() {
		
		return statistiquesRepository.findMaxDate();
	}
	
}
