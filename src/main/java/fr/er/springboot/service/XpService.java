package fr.er.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.User;
import fr.er.springboot.entity.Xp;
import fr.er.springboot.repository.XpRepository;
import fr.er.springboot.service.model.ModelService;

/**
 * @author erudo
 *
 */


@Service
public class XpService extends ModelService<Xp> {
	@Autowired
	private XpRepository xpRepository;

	@Override
	public Xp save(Xp object) {
		// TODO Auto-generated method stub
		isValidObject(object);
		
		return xpRepository.save(object);
	}

	public Xp one(Xp xp) {
		isValidObject(xp);
		
		return xpRepository.findById(xp.getId()).get();
	}

	@Override
	public List<Xp> saveAll(List<Xp> object) {
		
		isValidList(object);
		
		
		return xpRepository.saveAll(object);
	}

	@Override
	public List<Xp> findAll() {
		// TODO Auto-generated method stub
		return xpRepository.findAll();
	}

	
	public Xp findXpByUserAndProject(String user,String idProject) {
		isValidObject(user);
		isValidObject(idProject);
		
		return xpRepository.findByUser_loginAndProject_id(user, idProject);
	}
	
	
	public List<Xp> findXpByUser(String login) {
		isValidObject(login);
	
		return xpRepository.findByUser_login(login);
	}
	
	
	public List<Xp> findXpByProject(String idProject) {
		isValidObject(idProject);
		
		return xpRepository.findByProject_id(idProject);
	}


}
