/**
 * 
 */
package fr.er.springboot.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import fr.er.springboot.check.CheckData;
import fr.er.springboot.constant.Metric;
import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.Statistiques;
import fr.er.springboot.entity.User;
import fr.er.springboot.entity.Xp;
import fr.er.springboot.http.HttpApiRest;

/**
 * @author erudo
 *
 */

@Service
public class ApiService implements CommandLineRunner {
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private AnalyseService analyseService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StatistiquesService statistiquesService;
	
	@Autowired
	private XpService xpService;
	
	private HttpApiRest httpApiRest = new HttpApiRest();
	
	private CheckData checkdataUser = new CheckData();
	
	@Value("${user.token}")
	private String login;
	
	


	@Override
	public void run(String... args) throws Exception {		
			
			startApi();
			System.out.println("Injection Réussi ! ");
	}
	
	public void startApi() {
		httpApiRest.setUsername(login); 
		
		List<Project> projectFromDatabase = projectService.findAll();
		List<Project> projectFromAPI = httpApiRest.getProjectFromAPI();
		// Renvoie les données qui ne sont pas présentes en base
		List<Project> projectsToInsert = checkdataUser.checkIdenticalData(projectFromDatabase, projectFromAPI);
		
		List<User> userFromDatabase = userService.findAll();
		List<User> userFromAPI = httpApiRest.getUserFromAPI();
		List<User> userToInsert = checkdataUser.checkIdenticalData(userFromDatabase, userFromAPI);
		
		List<Analyse> analyseFromDatabase = analyseService.findAll();
	    List<Analyse> analyseFromAPI = httpApiRest.getAnalyseFromAPI();
	    List<Analyse> analyserToInsert = checkdataUser.checkIdenticalData(analyseFromDatabase, analyseFromAPI);
	    
	    System.out.println(analyserToInsert);
	
	    if (projectsToInsert.size() > 0) {
	    	projectService.saveAll(projectsToInsert);
		}
	    
	    if (userToInsert.size() > 0) {
	    	 userService.saveAll(userToInsert);
		}
	    
	    if (analyserToInsert.size() > 0) {
	    	analyseService.saveAll(analyserToInsert);
		}
	   
	   
	    List<Statistiques> statExit = statistiquesService.findAll();
	    List<Statistiques> statistiqueToCreateXpOnUser  = null;
	    /* si il n'y a pas de statistiques en base
	     * Alors on récupére toutes les analyses et on crée les statistiques a partir de la table analyse
	     */
	    if (statExit.size() == 0) {
	
	    	statistiqueToCreateXpOnUser   =  statistiquesService.saveAll(statistiquesService.createStatistiqueWithTableAnalyseWithoutDate()); 
			
		}else {
			/**
			 * Sinon on recherche la plus vieille statistiques existantes qui na vous permettre
			 * de Faire nos requêtes pour créer nos nouvelles statistique a partir des analyses
			 */
			Timestamp maxDate = statistiquesService.findMaxDate().getAnalysis_date();
			statistiqueToCreateXpOnUser =  statistiquesService.saveAll(statistiquesService.createStatistiqueWithTableAnalyseWithDate(maxDate)); 

		}
	    	    
	      		
	    List<Xp> xpFromDatabase = xpService.findAll();
	    System.out.println("Size xp de la abse " + xpFromDatabase.size());
	    List<Xp> xpList = new ArrayList<>();
	    
	    // Attribution de l'xp par rapport au metric(bugs,vulnerabilities..)
	    for (Statistiques statistiques : statistiqueToCreateXpOnUser) {
	    	int resultXpVulnerabilitie = statistiques.getNbVulnerabilitie() * Metric.VULNERABILITIES.getXp();
	    	int resultXpBugs = statistiques.getNbBugs() * Metric.BUGS.getXp();
	    	int resultXpCodeSmells = statistiques.getNbCode_smell() * Metric.CODE_SMELLS.getXp();
	    	
	    	int xpRegisterToDatabase = 0;
	    	int idXp = 0;
	    	
	    	/**
	    	 * Si un xp existe déjà alors on lui met a jour son xp par rapport aux données en récupérant son id
	    	 */
	    	for (Xp xp : xpFromDatabase) {
	    		if (xp.getProject().equals(statistiques.getProject()) && xp.getUser().equals(statistiques.getUser())) {
					
	    			xpRegisterToDatabase = xp.getXp();
	    			idXp = xp.getId();
	    			break;
	    			
				}
			}
	    	int xpTest = resultXpVulnerabilitie + resultXpBugs + resultXpCodeSmells;
	 
	    	// S'il y a une xp differente que celle en base
	    	if (xpTest != xpRegisterToDatabase) {
	    		int xpTotal =  xpTest + xpRegisterToDatabase;
	    		xpList.add(new Xp(idXp,xpTotal, statistiques.getAnalysis_date(),statistiques.getUser(), statistiques.getProject()));
			}
	 
	    	
	    }
	    
	   if (xpList.size() != 0) {
		   xpService.saveAll(xpList);
		   
    	}
	   
	}

}
