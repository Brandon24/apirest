/**
 * 
 */
package fr.er.springboot.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.error.TechnicalRuntimeException;
import fr.er.springboot.key.ErrorKey;
import fr.er.springboot.repository.AnalyseRepository;
import fr.er.springboot.service.model.ModelService;

 
/**
 * @author erudo
 *
 */
@Service
public class AnalyseService extends ModelService<Analyse> {
	@Autowired
	private AnalyseRepository analyseRepository;

	@Override
	public Analyse save(Analyse object) {
		isValidObject(object);
		return analyseRepository.save(object);
	}


	@Override
	public Analyse one(Analyse object) {
		isValidObject(object);
		
		return analyseRepository.findById(object.getId()).get();
	}

	@Override
	public List<Analyse> saveAll(List<Analyse> object) {
		isValidList(object);
	
		return analyseRepository.saveAll(object);
	}

	@Override
	public List<Analyse> findAll() {
		// TODO Auto-generated method stub
		return analyseRepository.findAll();
	}

	

}
