/**
 * 
 */
package fr.er.springboot.constant;

/**
 * @author erudo
 *
 */
public enum Metric {

	// Si la personne enleve des tests unitaire il obtiendra une valeur negative - et - = + pour ne pas avoir  de l'xp negative :)
	TESTS,VULNERABILITIES(-2),BUGS(-3),CODE_SMELLS(-2),ALERT_STATUS,COVERAGE;
	
	private int xp;
	
	Metric(){
		
	}
	
	Metric(int x) {
		this.xp = x;
		
	}

	/**
	 * @return the xp
	 */
	public  int getXp() {
		return xp;
	}
	
}
