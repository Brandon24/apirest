/**
 * 
 */
package fr.er.springboot.key;

/**
 * @author erudo
 *
 */
public class ErrorKey {
	public static final String nullList = "Une liste ne peut être nul ! ";
	public static final String listIsEmpty = "Une liste ne peut contenir 0 élément pour une insertion ! ";
	public static final String nullObject = " Object null ! Insertion impossible ! ";
	public static final String urlError = "URL FORMAT INVALIDE";
	public static final String ObjectType = "Les types d'objets sont différents ! ";

}
