/**
 * 
 */
package fr.er.springboot.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author erudo
 *
 */
public class Transformer {

	/**
	 * 
	 * @param <T>
	 * @param json
	 * @param desiredClass on passe la classe qui s'occupe du parsage du json correspondant(Nos Wrappers) pour récuperer un objet bien formé
	 * @return un dto de la classe passé en parametre
	 */
	public Object transformJsonToDTO(String json,Class<?> desiredClass) {
		ObjectMapper objectMapper = new ObjectMapper();
		Object object = null;
		Class<?> class1 = desiredClass;
		
		try { 
			//object.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
			objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
			object = objectMapper.readValue(json, class1);
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return object;
	}
	
}
