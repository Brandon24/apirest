/**
 * 
 */
package fr.er.springboot.resource;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.er.springboot.entity.Analyse;
import fr.er.springboot.entity.Project;
import fr.er.springboot.service.AnalyseService;

/**
 * @author erudo
 *
 */
@Controller
@Transactional
public class AnalyseRessource {
	
	@Autowired
	private AnalyseService analyseService;
	
	@GetMapping("/analyses/all")
    public ResponseEntity<List<Analyse>> findAllAnalyses() { 
		return new ResponseEntity<List<Analyse>>(analyseService.findAll(),HttpStatus.OK);
	}

	
//	@GetMapping("/analyses/one")
//    public ResponseEntity<Analyse> findOneProject(@RequestParam("name")String nameProject) {
//
//		return new ResponseEntity <Analyse>(analyseService.fin,HttpStatus.OK);
//	}
//	
}
