/**
 * 
 */
package fr.er.springboot.resource;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import fr.er.springboot.entity.Project;
import fr.er.springboot.entity.Statistiques;
import fr.er.springboot.service.ProjectService;
import fr.er.springboot.service.StatistiquesService;

/**
 * @author erudo
 *
 */
@Controller
@Transactional
public class StatistiquesRessource {

	@Autowired
	private StatistiquesService statsService;
	
	@GetMapping("/statistiques/all")
    public ResponseEntity<List<Statistiques>> findAllStatistiques() {
	
		return new ResponseEntity<List<Statistiques>>(statsService.findAll(),HttpStatus.OK);
	}
}
