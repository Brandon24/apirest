/**
 * 
 */
package fr.er.springboot.resource;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.er.springboot.entity.Xp;
import fr.er.springboot.service.XpService;

/**
 * @author erudo
 *
 */
@Controller
@Transactional
public class XpRessource {
	@Autowired
	private XpService xpService;

	
	
	@GetMapping("/xp/all")
    public ResponseEntity<List<Xp>> findAllXp() {
	
       return new ResponseEntity<List<Xp>>(xpService.findAll(),HttpStatus.OK);
	}
	
	@GetMapping("/xp/one/project")
    public ResponseEntity<List<Xp>> findAllXpByProject(@RequestParam("idProject") String idProject) {
		System.out.println(idProject);
		return new ResponseEntity<List<Xp>>(xpService.findXpByProject(idProject),HttpStatus.OK);
	}
	
	
	@GetMapping("/xp/one/user")
    public ResponseEntity<List<Xp>> findAllXpByUser(@RequestParam("userLogin")String userLogin) {
	
		return new ResponseEntity<List<Xp>>(xpService.findXpByUser(userLogin),HttpStatus.OK);
	}
	
	@GetMapping("/xp/one/user/project")
    public ResponseEntity<Xp> findOneXpByUserAndProject(@RequestParam("userLogin")String userLogin,@RequestParam("idProject") String idProject) {
	
		return new ResponseEntity<Xp>(xpService.findXpByUserAndProject(userLogin, idProject),HttpStatus.OK);
	}

}
