/**
 * 
 */
package fr.er.springboot.resource;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import fr.er.springboot.entity.User;
import fr.er.springboot.service.UserService;

/**
 * @author erudo
 *
 */
@Controller
@Transactional
public class UserRessource {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/users/all")
    public ResponseEntity<List<User>> findAllUsers() { 

		return new ResponseEntity<List<User>>(userService.findAll(),HttpStatus.OK);
	}

}
