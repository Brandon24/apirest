/**
 * 
 */
package fr.er.springboot.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.er.springboot.entity.Project;
import fr.er.springboot.service.ProjectService;
import javassist.expr.NewArray;

/**
 * @author erudo
 *
 */


@Controller
public class ProjectResource {
	@Autowired
	private ProjectService projectService;
	
	@GetMapping("/projects/all")
    public ResponseEntity<List<Project>> findAllProjects() {

		return new ResponseEntity <List<Project>>(projectService.findAll(),HttpStatus.OK);
	}
	

	
	@GetMapping("/projects/one")
    public ResponseEntity<Project> findOneProject(@RequestParam("name")String nameProject) {

		return new ResponseEntity <Project>(projectService.findOneProjectByName(nameProject),HttpStatus.OK);
	}
	
}
